# ready to run example: PythonClient/multirotor/hello_drone.py
import airsim
import  os
import cv2
import numpy as np
import time
import matplotlib.pyplot as plt
import math
# connect to the AirSim simulator
client = airsim.MultirotorClient()
# client.reset()
client.confirmConnection()
client.enableApiControl(True)

client.armDisarm(True)
client.reset()
# objects = client.simListSceneObjects()
# # print(objects)

# # # Async methods returns Future. Call join() to wait for task to complete.
# # # factor = math.pi/180
# client.takeoffAsync().join()
# # # client.moveOnSplineVelConstraintsAsync('Track_2')
# # # moveByAngleRatesThrottleAsync(roll_rate, pitch_rate, yaw_rate, throttle, duration, vehicle_name='')
# angle = 0 
# disp = -10

# client.moveToPositionAsync(0, -3, -2, 1).join()
# a = math.pi/2
# client.moveByAngleRatesZAsync(0,0,a, -2, 1).join()
# angle += a
# disp_x, disp_y = disp*math.sin(angle), disp*math.cos(angle)
# client.moveToPositionAsync(disp_x, disp_y, -2, 1).join()
# # time.sleep(2)

# # # client.moveToPositionAsync(0, -10, -2, 0.5)
# # client.moveByAngleRatesZAsync(0,0,0.34, -2, 1)



# drone Position
# tracker bounding box lefttop x,y


# while (1):
#     # y = y+5
#     client.moveToPositionAsync(0, -5, -5, 5).join()
#     time.sleep(2)
# start = time.time()
# responses = client.simGetImages([
#         airsim.ImageRequest(0, airsim.ImageType.Segmentation, False, False),
#         airsim.ImageRequest(0, airsim.ImageType.Scene, False, False),
#         airsim.ImageRequest(0, airsim.ImageType.DepthPlanner, True, False),
#         ])
# get_seg, get_img, get_depth = responses[0], responses[1], responses[2]
# print("Time taken to get images: ", time.time()-start)
# # Segmenation #
# seg_1d = np.frombuffer(get_seg.image_data_uint8, dtype=np.uint8) 
# # reshape array to 4 channel image array H X W X 4
# seg_rgb = seg_1d.reshape(get_seg.height, get_seg.width, 3)

# # Original Image #
# img1d = np.frombuffer(get_img.image_data_uint8, dtype=np.uint8) 
# # reshape array to 4 channel image array H X W X 4
# img_rgb = img1d.reshape(get_img.height, get_img.width, 3)

# # print(get_depth.image_data_uint8)
# depth = airsim.list_to_2d_float_array(get_depth.image_data_float, get_depth.height, get_depth.width) 
# print(get_depth.height, get_depth.width)
# depth = depth.reshape(get_depth.height, get_depth.width, 1)
# # depth = depth.reshape(get_depth.height, get_depth.width, 3)
# # depth = np.array(depth * 255, dtype=np.uint8)

# depth_8bit_lerped = np.interp(depth, (0, 50), (0, 255)).astype('uint8')
# # print(depth_8bit_lerped)
# plt.imshow(depth_8bit_lerped)
# plt.show()
# # cv2.waitKey(0)
# # cv2.destroyAllWindows()
