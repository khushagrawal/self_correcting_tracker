# Self Correcting Tracker
Repository to host code for self correcting tracking mechanism and its evaluation against baseline CV methods.


## Instructions

### Installation
- Make a new virtual environment (Optional but recommeneded)
- Run `pip3 install -r requirements.txt`
- Make sure you don't have `opencv-python`. Use ONLY `opencv-contrib-python`, otherwise your code may throw some error related to trackers.
- You can optionally accelerate code using cython. Refer [OPTIONAL: Cython acceleration](#optional-cython-acceleration) 

#### OPTIONAL: Cython acceleration

Instead of using normal python code use cython for speed boosts (1.5 times+). You will need to build this from source!

```bash
# cd to your preferred directory and clone this repo
git clone https://github.com/KaiyangZhou/deep-person-reid.git

cd deep-person-reid/

# install torchreid (don't need to re-build it if you modify the source code)
python setup.py develop
```

### Dataset
Download the `VisDrone2019-SOT-val` SOT dataset from [link](https://drive.google.com/file/d/18SNAOlCJtApnG2m45ud-1e_OtGYill0D/view). Place the unzipped folder in `~/Downloads` directory. In case you have the unzipped dataset in a seperate folder, pass appropriate arguments while executing the python scripts. For help with any python script argument, execute `python name_of_the_script.py -h`.

### Evaluation
- Execute `python run_baselines.py` command to run baseline evaluations.
- Execute `python run_sct.py` command to run evaluations for sct mechanism.
- Use the `visualize.ipynb` notebook to view & save evaluation results.

## Pushing Code
- Clear all outputs before pushing any `.ipynb` file.
- Try commenting important blocks of code. 
- Maintain indentation and spacing syle throught.
- Don't push unnecessary files. Eg: test videos, debugging files, result csv files, etc. 


## Contributers
- [Rohit Lal](http://take2rohit.github.io/)
- [Khush Agrawal](https://khush3.github.io)
- [Himanshu Patil](https://www.linkedin.com/in/hipatil/)
