import os
import cv2 
import shutil
import csv
import argparse
import numpy as np
import pandas as pd


def read_gt(path):
    '''Read the ground truth file from the give path'''
    gt = []
    with open(path) as f:
        c = csv.reader(f, delimiter=',')
        for row in c:
            gt.append(row)
    gt = np.array(gt).astype(np.int)
    return gt


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Create dataset for training re-id dataset from the VisDrone Dataset')
    parser.add_argument("-r", "--read_path", type=str, default='/home/rex/Downloads/VisDrone2019-MOT-val/', help='Path the VisDrone dataset')
    parser.add_argument("-s", "--save_path", type=str, default='dataset_reid_mot', help='Path for saving processed dataset')
    parser.add_argument("-d", "--dimension", type=int, nargs='+', default=None, help='height width for the images to be saved')
    parser.add_argument("-c", "--class_name", type=str, default='person', help='Class name for the processed dataset')
    # parser.add_argument("-v", "--invalid_sequences", type=str, nargs='+', default=['uav0000003_00000_s', 'uav0000090_01104_s', 'uav0000091_01288_s', 'uav0000076_00241_s', 'uav0000090_00276_s', 'uav0000091_00460_s', 'uav0000107_01763_s', 'uav0000089_00920_s', 'uav0000091_01035_s', 'uav0000091_02530_s', 'uav0000126_07915_s'])

    args = parser.parse_args()

    if not os.path.exists(args.save_path): os.mkdir(args.save_path)

    seq_dir = os.path.join(args.read_path, 'sequences')
    sequences = os.listdir(seq_dir)
    identity = 0

    classes = {
        'regions': 0, 
        'pedestrian': 1, 
        'people': 2, 
        'bicycle': 3, 
        'car': 4,
        'van': 5, 
        'truck': 6, 
        'tricycle': 7, 
        'awning-tricycle': 8, 
        'bus': 9, 
        'motor': 10, 
        'others': 11
    }

    for seq in sequences:

        print('Processing: %s'%seq)
        save_path_seq = os.path.join(args.save_path, str(seq))

        annotation_file = os.path.join(args.read_path, 'annotations', seq + '.txt')
        labels = pd.read_csv(annotation_file, names=['frame_index','target_id','bbox_left','bbox_top','bbox_width','bbox_height','score','object_category','truncation','occlusion'])
        labels.head()
        
        seqlen = len(os.listdir(os.path.join(seq_dir, seq)))
        for index in range(1, seqlen):

            img_file = os.path.join(seq_dir, seq, '%07d.jpg'%index)
            frame = cv2.imread(img_file)

            labels_filtered = labels[(labels['frame_index'] == index) & ((labels['object_category'] == classes['pedestrian']) | (labels['object_category'] == classes['people']))]
            
            for obj in labels_filtered.index:

                x, y, w, h = labels_filtered["bbox_left"][obj], labels_filtered["bbox_top"][obj], labels_filtered["bbox_width"][obj], labels_filtered["bbox_height"][obj]
                cropped_frame = frame[y:y+h, x:x+w, :]
                
                if args.dimension is not None:
                    height, width = args.dimension
                    cropped_frame = cv2.resize(cropped_frame, (width, height))
                
                save_dir = save_path_seq + '_' + str(labels_filtered['target_id'][obj])
                if not os.path.exists(save_dir): os.mkdir(save_dir)
                save_path = os.path.join(save_dir, args.class_name + '_' + str(index) + '.jpg')

                cv2.imwrite(save_path, cropped_frame)

    print('Done!')


