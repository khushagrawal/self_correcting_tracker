import csv
import numpy as np
import matplotlib.pyplot as plt

def read_csv(path):
    '''Read the ground truth file from the give path'''
    gt = []
    with open(path) as f:
        c = csv.reader(f, delimiter=',')
        for row in c:
            gt.append(row)
    return np.array(gt[1:]).astype(np.float)

mlfn = read_csv('C:/Users/hipat/Desktop/csv_acc_and_loss_reid/mlfn/run-.-tag-Train_acc.csv')
mobile_net = read_csv('C:/Users/hipat/Desktop/csv_acc_and_loss_reid/mobilenetv2_x1_4/run-.-tag-Train_acc.csv')
osnet_ain_x1_0 = read_csv('C:/Users/hipat/Desktop/csv_acc_and_loss_reid/osnet_ain_x1_0/run-.-tag-Train_acc.csv')
osnet_ibn_x1_0 = read_csv('C:/Users/hipat/Desktop/csv_acc_and_loss_reid/osnet_ibn_x1_0/run-.-tag-Train_acc.csv')
resnet50 = read_csv('C:/Users/hipat/Desktop/csv_acc_and_loss_reid/resnet50/run-.-tag-Train_acc.csv')
shufflenet = read_csv('C:/Users/hipat/Desktop/csv_acc_and_loss_reid/shufflenet/run-.-tag-Train_acc.csv')


plt.plot(mlfn[:,2],             label = 'mlfn:              32.5M')
plt.plot(mobile_net[:,2],       label = 'mobile_net:        4.3M')
plt.plot(osnet_ain_x1_0[:,2],   label = 'osnet_ain_x1_0:    2.2M')
plt.plot(osnet_ibn_x1_0[:,2],   label = 'osnet_ibn_x1_0:    2.2M')
plt.plot(resnet50[:,2],         label = 'resnet50:          23.5M')
plt.plot(shufflenet[:,2],       label = 'shufflenetv1:      3.4M')
plt.legend()
plt.show()