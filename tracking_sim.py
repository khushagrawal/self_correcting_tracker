import os
import cv2 
import csv
import time
import json
import argparse
import numpy as np
from sct import SCT
import pandas as pd
import evaluation_helpers as eh
from tqdm import tqdm


if __name__ == '__main__':

    with open('configs/tracker.json', 'r') as f:
        cfgs_tracker = json.load(f)

    with open('configs/reid_test.json', 'r') as f:
        cfgs_reid = json.load(f)

    with open('configs/tracker_eval.json', 'r') as f:
        cfgs = json.load(f)

    TRACKERS = {
        "csrt": cv2.TrackerCSRT_create,
        "kcf": cv2.TrackerKCF_create,
        "boosting": cv2.TrackerBoosting_create,
        "mil": cv2.TrackerMIL_create,
        "tld": cv2.TrackerTLD_create,
        "medianflow": cv2.TrackerMedianFlow_create,
        "mosse": cv2.TrackerMOSSE_create,
        "DeepSCT": SCT,
        "goturn":cv2.TrackerGOTURN
    }

    if cfgs['type'] == 'SOT':
        gt_parser = eh.read_gt_sot
    else:
        gt_parser = eh.read_gt_mot

    if cfgs['algorithm'] == 'all': algorithms, algorithm_names = TRACKERS.values(), TRACKERS.keys()
    else: algorithms, algorithm_names = [TRACKERS[algorithm] for algorithm in cfgs['algorithm']],  cfgs['algorithm']

    if not os.path.exists(cfgs['save_path']): os.mkdir(cfgs['save_path'])

    sequences = os.path.join(cfgs['read_path'], 'sequences')
    no_seq = cfgs['no_seq'] if cfgs['no_seq'] > 0 else len(sequences)

    if cfgs['eval_sequences'] == 'random':
        eval_sequences = np.random.choice(os.listdir(sequences), no_seq)
    else:
        eval_sequences = cfgs['eval_sequences']

    for sequence in eval_sequences:
        img_dir = os.path.join(sequences, sequence)
        seqlen = len(os.listdir(img_dir))
        gt = gt_parser(os.path.join(cfgs['read_path'], 'annotations', sequence) + '.txt', seqlen)

        save_path_base = os.path.join(cfgs['save_path'], sequence)
        if not os.path.exists(save_path_base): os.mkdir(save_path_base)

        print('Test sequence: %s'%sequence)

        for algorithm_name, algorithm in tqdm(zip(algorithm_names, algorithms), desc='Evaluating: %s'%sequence):
            print('Testing algorithm: %s'%algorithm_name)

            tracker = algorithm() if not algorithm_name == 'sct' else algorithm(cfgs_tracker, cfgs_reid['reid'])
            start = True
            trajectory = []
            inference_times = []

            save_path_algorithm = os.path.join(save_path_base, algorithm_name)

            for frame_number in range(1, seqlen):
                if cfgs['type'] == 'SOT':
                    img_file = os.path.join(img_dir, 'img%07d.jpg'%frame_number)
                else:
                    img_file = os.path.join(img_dir, '%07d.jpg'%frame_number)

                # print(img_file)
                frame = cv2.imread(img_file)
                gt_coords = gt[frame_number]

                start_time = time.time()
                if start:
                    x1, y1, w, h = gt_coords
                    trajectory.append((x1, y1, w, h))

                    x1, y1, x2, y2 = int(x1), int(y1), int(x1 + w), int(y1 + h)

                    tracker.init(frame, (x1, y1, x2 - x1, y2 - y1))
                    start = False
                    frame_width, frame_height = frame.shape[1], frame.shape[0]
                    
                    size = (frame_width, frame_height) 
                    
                    if cfgs['save_video']: 
                        result = cv2.VideoWriter(save_path_algorithm + '_visual.avi', cv2.VideoWriter_fourcc(*'MJPG'), 30, size) 
                
                if frame is None:
                    print('Received None type image: %s'%frame_number)
                    continue

                success, (x1, y1, w, h) = tracker.update(frame)
                x1, y1, x2, y2 = int(x1), int(y1), int(x1 + w), int(y1 + h)
                trajectory.append((x1, y1, w, h))

                inference_time = time.time() - start_time
                inference_times.append(inference_time)

                if cfgs['save_video']: 
                    res_img = eh.show_results(frame, int(1/inference_time), frame_number, (x1, y1, x2, y2), coordinates_gt=gt_coords,algorithm=algorithm_name)
                    result.write(res_img)

            np.savetxt(save_path_algorithm + '_trajectory.csv', trajectory, delimiter=",", fmt='%d')
            np.savetxt(save_path_algorithm + '_inference_time.csv', inference_times, delimiter=",")

        cv2.destroyAllWindows()
        
    if cfgs['save_video']: result.release()
        