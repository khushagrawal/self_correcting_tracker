import cv2
import numpy as np

def seg2bbox(seg_image,show_image=False):
    '''
    @Params
        seg_image : input semantic segmentation map
        show_image : show bounding box on segmentation map

    @Returns
        Coordinates of bounding box (Top left corner and right bottom corner)
        Segmentation map with bounding box
    '''

    lowerRange = np.array([14,7,72])
    upperRange = np.array([16,9,74])
    mask = cv2.inRange(seg_image, lowerRange,upperRange)

    r_white_ar, c_white_ar = np.where(mask == 255)
    r_white_ar.sort()
    c_white_ar.sort()
    rLow, cLow, rHigh, cHigh = r_white_ar[0], c_white_ar[0], r_white_ar[-1], c_white_ar[-1]

    x_top, y_top, x_bottom, y_bottom = cLow,rLow, cHigh,rHigh
    cv2.rectangle(seg_image,(x_top, y_top), (x_bottom, y_bottom), [255,255,255])

    if show_image:
        cv2.namedWindow('', cv2.WINDOW_NORMAL)
        cv2.imshow('', seg_image)
        cv2.waitKey(0)

    return [x_top, y_top, x_bottom, y_bottom], seg_image



if __name__ == '__main__':
    seg_image_path = 'API_control1.png'
    seg_image = cv2.imread(seg_image_path)
    bbox, image_with_bbox = seg2bbox(seg_image,show_image=True)
