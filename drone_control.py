import  os
import cv2
import csv
import time
import json
import airsim
import numpy as np
import pandas as pd
from sct import SCT
from AirSimSegMap_to_bbox import seg2bbox
import math


def get_image(client):
    responses = client.simGetImages([
        airsim.ImageRequest(0, airsim.ImageType.Scene, False, False),
        airsim.ImageRequest(0, airsim.ImageType.Segmentation, False, False),
        # airsim.ImageRequest(0, airsim.ImageType.DepthPlanner, True, False),
        ])
    get_img, get_seg = responses[0], responses[1]

    # Original Image #
    img1d = np.frombuffer(get_img.image_data_uint8, dtype=np.uint8) 
    img_rgb = img1d.reshape(get_img.height, get_img.width, 3) # reshape array to 4 channel image array H X W X 4

    seg_1d = np.frombuffer(get_seg.image_data_uint8, dtype=np.uint8) 
    segment = seg_1d.reshape(get_seg.height, get_seg.width, 3)  # reshape array to 4 channel image array H X W X 4
    # depth = airsim.list_to_2d_float_array(get_depth.image_data_float, get_depth.height, get_depth.width)
    # # print(depth) 
    # # print(get_depth.height, get_depth.width)
    # depth = depth.reshape(get_depth.height, get_depth.width, 1)
    # depth_8bit_lerped = np.interp(depth, (0, 50), (0, 255)).astype('uint8')
    return img_rgb, segment #, depth


def drone_init():
    # connect to the AirSim simulator
    client = airsim.MultirotorClient()
    client.confirmConnection()
    client.enableApiControl(True)
    client.armDisarm(True)
    objects = client.simListSceneObjects()
    # Async methods returns Future. Call join() to wait for task to complete.
    client.takeoffAsync().join()


    success1 = client.simSetSegmentationObjectID("ai_2", 20)
    success2 = client.simSetSegmentationObjectID("ai2_2", 40)
    success3 = client.simSetSegmentationObjectID("ai3_2", 60)
    print(success1,success2,success3)
    return client


class Trajectory:
    def __init__(self, base_path, algorithm_name):
        self.trajectory_tracker = []
        self.trajectory_gt = []
        self.base_path = base_path
        self.algorithm_name = algorithm_name

    def update(self, t, gt):
        self.trajectory_tracker.append(t)
        self.trajectory_gt.append(gt)

    def save(self):
        np.savetxt(os.path.join(self.base_path, self.algorithm_name + '_trajectory.csv'), self.trajectory_tracker, delimiter=",", fmt='%d')
        np.savetxt(os.path.join(self.base_path, self.algorithm_name + '_trajectory_gt.csv'), self.trajectory_gt, delimiter=",", fmt='%d')


if __name__ == '__main__':
    start = time.time()

    with open('configs/tracker.json', 'r') as f:
        cfgs_tracker = json.load(f)
    with open('configs/reid_test.json', 'r') as f:
        cfgs_reid = json.load(f)
    with open('configs/tracker_eval.json', 'r') as f:
        cfgs = json.load(f)

    TRACKERS = {
        "csrt": cv2.TrackerCSRT_create,
        "kcf": cv2.TrackerKCF_create,
        "boosting": cv2.TrackerBoosting_create,
        "mil": cv2.TrackerMIL_create,
        "tld": cv2.TrackerTLD_create,
        "medianflow": cv2.TrackerMedianFlow_create,
        "mosse": cv2.TrackerMOSSE_create,
        "DeepSCT": SCT,
        "goturn":cv2.TrackerGOTURN
    }
    base_path = 'D:/Work/sct/self_correcting_tracker/'

    if cfgs['algorithm'] == 'all': algorithms, algorithm_names = TRACKERS.values(), TRACKERS.keys()
    else: algorithms, algorithm_names = [TRACKERS[algorithm] for algorithm in cfgs['algorithm']],  cfgs['algorithm']

    algorithm_name, algorithm = algorithm_names[0], algorithms[0]

    tracker = algorithm() if not algorithm_name == 'DeepSCT' else algorithm(cfgs_tracker, cfgs_reid['reid'])
    print("JSON file loaded and tracker initialized\n")

    client = drone_init()
    print("Reached Position\n")
    print("Time taken to takeoff and get ready: ", time.time()-start)

    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter('csrt.avi', fourcc, 20.0, (320,  240))
    # Get Images and GT from segmentation 
    img, segment = get_image(client)
    
    (x1_gt, y1_gt, x2_gt, y2_gt), detect_img = seg2bbox(segment)
    # x1_gt, y1_gt, x2_gt, y2_gt = int(x1_gt*480/256), int(y1_gt*360/144), int(x2_gt*480/256), int(y2_gt*360/144)
    tracker.init(img, (x1_gt, y1_gt, x2_gt - x1_gt, y2_gt - y1_gt))
    
    print("\nPerson to be tracked recorded")

    depth_person = 0
    x_person = 0
    angle = 0

    if not os.path.exists(base_path): os.mkdir(base_path)
    trajectory = Trajectory(base_path, algorithm_name)

    while(True):
        start = time.time()
        img, segment = get_image(client)
        position = client.simGetObjectPose('ai_2').position
        print("FPS: ",1/(time.time()-start))

        (x1_gt, y1_gt, x2_gt, y2_gt), detect_img = seg2bbox(segment)
        success, (x1, y1, w, h) = tracker.update(img)    # 256 x 144, but required 480 x 360
        (x1, y1, w, h) = (int(x1), int(y1), int(w), int(h)) 

        trajectory.update((x1, y1, w, h), (x1_gt, y1_gt, x2_gt - x1_gt, y2_gt - y1_gt))
        cv2.rectangle(img, (x1, y1), (x1+w, y1+h), [0,255,0])
        cv2.rectangle(img, (x1_gt, y1_gt), (x2_gt, y2_gt), [0,0,255])

        out.write(img)
        cv2.imshow("Image", img)
        cv2.waitKey(1)
        print("Able to identify or not:", success)

        midpoint_x, midpoint_y = int(x1+w/2), int(y1+h/2)
        depth_person = position.y_val
        print(depth_person)
        # depth_person += depth[midpoint_y][midpoint_x][0]
        
        print("Width of Image: ", img.shape[1])
        print("Midpoint of BB: ", midpoint_x)
        x_trans = midpoint_x - img.shape[1]/2
        print("rotate drone: ", x_trans)
                
        # client.moveByAngleRatesZAsync(0, 0, rotate_drone, -2, 1).join()
        # trajectory.save()
        client.moveToPositionAsync(x_trans, depth_person+5, -2, 1.5)
    out.release()   
    cv2.destroyAllWindows()
