import cv2 
import numpy as np
from reid import ReId
from pytorchYOLOv4.darknet2pytorch import YoloV4init


def get_converted_coords(pre_boxes, prev_box_to_track, H, W):
    p_boxes = []
    for box in pre_boxes:
        if box[-1] == 0:
            box = box[:4] * np.array([W, H, W, H])
            (x_center, y_center, w, h) = box.astype("int")
            x = int(x_center - w/2)
            y = int(y_center - h/2)
            box = (x, y, w, h)
            p_boxes.append(box)
        boxes = p_boxes
    box = np.linalg.norm(np.array(boxes)-np.array(prev_box_to_track[0]), axis=1)
    distances = sorted(box)

    box_to_track = [boxes[np.where(box == distances[0])[0][0]]][0]
    return (x, y, w, h), box_to_track


class SCT():
    def __init__(self, cfgs, cfgs_reid):

        self.Trackers = {
            "csrt": cv2.TrackerCSRT_create,
            "kcf": cv2.TrackerKCF_create,
            "boosting": cv2.TrackerBoosting_create,
            "mil": cv2.TrackerMIL_create,
            "tld": cv2.TrackerTLD_create,
            "medianflow": cv2.TrackerMedianFlow_create,
            "mosse": cv2.TrackerMOSSE_create
        }

        self.cfgs = cfgs
        self.reid = ReId(cfgs_reid)
        self.algorithm = self.Trackers[self.cfgs['algorithm']]
        self.yolov4 = YoloV4init(self.cfgs['cfg_v4_person'], self.cfgs['weight_v4_person'], use_cuda=self.cfgs['use_cuda'], class_to_detect=['person'])


    def init(self, frame, gt_coords):
        self.frame_number = 0
        x, y, w, h = gt_coords
        x, y, w, h = int(x), int(y), int (w), int(h)
        initial_coords = (x, y, w, h)

        self.tracker = self.algorithm()
        self.tracker.init(frame, (x, y, w, h))
        self.box_to_track = (initial_coords)
        self.reid.update(frame[y:y+h, x:x+w, :])


    def update(self, frame):
        (H,W) = frame.shape[:2]

        if self.frame_number % self.cfgs['skip_interval']:
            (success, self.box_to_track) = self.tracker.update(frame)

        else:
            if self.cfgs['debug']: print('Correcting')
            pre_boxes = self.yolov4.get_boxes(frame)
            detection_boxes = self.yolov4.get_converted_coords(pre_boxes[0], self.box_to_track, H, W)
            
            if self.cfgs['debug']:
                result = self.yolov4.plot_boxes(frame, pre_boxes) 
                cv2.imshow("YOLO - Detections", result)             
            best_box = self.find_closest(detection_boxes)

            x, y, w, h = best_box
            match, _, _ = self.reid(frame[y:y+h, x:x+w, :])

            if not match:
                id_diff = []
                for box in sorted(detection_boxes):
                    x, y, w, h = box
                    _, _, diff = self.reid(frame[y:y+h, x:x+w, :])
                    id_diff.append([box, diff])
                sorted_id_diff = sorted(id_diff, key=lambda a: a[1], reverse=True)
                best_box = sorted_id_diff[0][0]
                x, y, w, h = best_box            

            success = True
            self.box_to_track = best_box
            self.tracker = self.algorithm()
            self.tracker.init(frame, (x, y, w, h))
            # self.reid.update(frame[y:y+h, x:x+w, :])

        # if success:
        (x, y, w, h) = [int(v) for v in self.box_to_track] ###### Handling Failure cases is remaining!

        self.frame_number += 1
        return success, (x, y, w, h)


    def find_closest(self, boxes):
        iou = self.get_iou(boxes)
        iou_sorted = np.sort(iou)

        if (iou == 0).all():
            self.cfgs['skip_interval'] = max(self.cfgs['skip_interval']//2, self.cfgs['skip_interval_min'])
            box = np.linalg.norm(np.array(boxes) - np.array(self.box_to_track[0]), axis=1)
            distances = sorted(box)
            closest_box = [boxes[np.where(box == distances[0])[0][0]]][0]
        else:
            self.cfgs['skip_interval'] = min(self.cfgs['skip_interval']*2, self.cfgs['skip_interval_max'])
            closest_box = [boxes[np.where(iou == iou_sorted[-1])[0][0]]][0]
        if self.cfgs['debug']: print('Interval:', self.cfgs['skip_interval'])
        return closest_box


    def get_iou(self, boxes):
        ious = []
        boxA = (self.box_to_track[0], self.box_to_track[1], self.box_to_track[0] + self.box_to_track[2], self.box_to_track[1] + self.box_to_track[3])
        for box in boxes:
            boxB = (box[0], box[1], box[0] + box[2], box[1] + box[3])

            xA = max(boxA[0], boxB[0])
            yA = max(boxA[1], boxB[1])
            xB = min(boxA[2], boxB[2])
            yB = min(boxA[3], boxB[3])

            interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

            boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
            boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

            iou = interArea / float(boxAArea + boxBArea - interArea)

            ious.append(iou)
        return np.array(ious)
        



