from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
import os
import sys
import json
import torchreid
from torchreid.data import ImageDataset


class VisDroneDataset(ImageDataset):
    dataset_dir = 'visdrone_dataset'

    def __init__(self, root='', base_dir='dataset_reid', **kwargs):
        self.root = os.path.abspath(os.path.expanduser(root))
        self.dataset_dir = os.path.join(self.root, self.dataset_dir)

        self.load_vd_datasets(base_dir)

        super(VisDroneDataset, self).__init__(self.train_list, self.query_list, self.gallery_list, **kwargs)
    
    def load_vd_datasets(self, base_dir):
        self.train_list = []
        self.query_list = []
        self.gallery_list = []

        for pid, folder in enumerate(os.listdir(base_dir)):
            for file in os.listdir(os.path.join(base_dir, folder)):
                img_abs_path = os.path.join(base_dir, folder, file)
                self.train_list.append((img_abs_path, pid, 0))
                self.query_list.append((img_abs_path, pid, 1))
                self.gallery_list.append((img_abs_path, pid, 0))


if __name__ == '__main__':

    torchreid.data.register_image_dataset('visdrone_dataset', VisDroneDataset)

    with open('configs/reid_train.json', 'r') as f:
        cfg = json.load(f)


    datamanager = torchreid.data.ImageDataManager(
        root='reid-data',
        sources='visdrone_dataset',
        # combineall=True,
        batch_size_train=cfg['batch_size_train'], 
        batch_size_test=cfg['batch_size_test'],
        transforms=cfg['transforms']
    )

    model = torchreid.models.build_model(
        name=cfg['model_name'],
        num_classes=datamanager.num_train_pids,
        loss=cfg['train_loss'],
        pretrained=cfg['use_pretrained']
    )

    model = model.cuda()

    optimizer = torchreid.optim.build_optimizer(
        model,
        optim=cfg['optimizer'],
        lr=cfg['lr']
    )

    scheduler = torchreid.optim.build_lr_scheduler(
        optimizer,
        lr_scheduler='single_step',
        stepsize=cfg['scheduler_step_size']
    )

    engine = torchreid.engine.ImageSoftmaxEngine(
        datamanager,
        model,
        optimizer=optimizer,
        scheduler=scheduler,
        label_smooth=True
    )

    engine.run(
        save_dir=os.path.join(cfg['save_dir'], cfg['model_name']),
        max_epoch=cfg['epoch'],
        eval_freq=cfg['eval_freq'],
        print_freq=cfg['print_freq'],
        test_only=False
    )