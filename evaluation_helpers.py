import os
import cv2 
import csv
import time
import json
import argparse
import numpy as np
from sct import SCT
import pandas as pd
import matplotlib.pyplot as plt

def selection(event, x, y, flags, param):
    '''Function for obtaining the mouse click coordinates'''
    global select
    global first_select
    if event == cv2.EVENT_LBUTTONDOWN:
        pts.append((x,y))
        select = True
        first_select = True


def show_results(img, fps, frame_number, coordinates, coordinates_gt=None, color=(0,255,0), thickness=5, wait_time=1, algorithm=None):
    '''Display tracker results'''
    cv2.namedWindow(algorithm, cv2.WINDOW_NORMAL)
    x1, y1, x2, y2 = coordinates
    img = cv2.rectangle(img, (x1, y1), (x2, y2), color, thickness) 
    if coordinates_gt is not None:
        x1, y1, x2, y2 = coordinates_gt[0], coordinates_gt[1], coordinates_gt[0] + coordinates_gt[2], coordinates_gt[1] + coordinates_gt[3]
        img = cv2.rectangle(img, (x1, y1), (x2, y2), (0, 0, 255), thickness) 
    
    # cv2.putText(img, f"FPS: {fps}", (1600, 50), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 255), thickness=3)
    if algorithm is not None: cv2.putText(img, f"{algorithm}", (1600, 50), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 255), thickness=3)
    # cv2.putText(img, f"Frame: {frame_number}", (1500, 160), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 255), thickness=3)
    
    cv2.imshow(algorithm, img) 
    key = cv2.waitKey(wait_time)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()

    return img


def read_gt_sot(path, seqlen):
    '''Read the ground truth file from the give path'''
    gt = []
    with open(path) as f:
        c = csv.reader(f, delimiter=',')
        for row in c:
            gt.append(row)
    gt = np.array(gt).astype(np.int)
    return gt


def read_gt_mot(path, seqlen):
    '''Read the ground truth file from the give path'''
    classes = {
        'regions': 0, 
        'pedestrian': 1, 
        'people': 2, 
        'bicycle': 3, 
        'car': 4,
        'van': 5, 
        'truck': 6, 
        'tricycle': 7, 
        'awning-tricycle': 8, 
        'bus': 9, 
        'motor': 10, 
        'others': 11
    }
    gt = []
    labels = pd.read_csv(path, names=['frame_index','target_id','bbox_left','bbox_top','bbox_width','bbox_height','score','object_category','truncation','occlusion'])

    for index in range(1, seqlen):
        labels_filtered = labels[(labels['frame_index'] == index) & ((labels['object_category'] == classes['pedestrian']) | (labels['object_category'] == classes['people']))]
        for obj in labels_filtered.index:
            x, y, w, h = labels_filtered["bbox_left"][obj], labels_filtered["bbox_top"][obj], labels_filtered["bbox_width"][obj], labels_filtered["bbox_height"][obj]
            gt.append((x,y,w,h))
            break  
    return gt


def calc_score(p1, p2, yolo=False):
    '''Calculate IOU score for a gt, coord pair'''
    x11, y11, x12, y12 = p1
    if not yolo: p2 = p2[0], p2[1], p2[0] + p2[2], p2[1] + p2[3]
    x21, y21, x22, y22 = p2
    intersection = max((min(x12, x22) - max(x11, x21)), 0) * max((min(y12, y22) - max(y11, y21)), 0)
    union = ((x12 - x11) * (y12 - y11)) + ((x22 - x21) * (y22 - y21)) - intersection
    iou = intersection * 1.0 / union
    return iou


def get_success_precision(tracker_traj, gt_traj, threshold_precision, threshold_success, yolo=False):
    """
    Definitions
    Success is 
    Precision is the average Euclidean distance between the center locations of the tracked targets and the manually labeled ground truths.
    """
    success = []
    precision = []
    for tracker_bb, gt_bb in zip(tracker_traj, gt_traj):
        if not yolo: 
            tracker_bb = tracker_bb[0], tracker_bb[1], tracker_bb[0] + tracker_bb[2], tracker_bb[1] + tracker_bb[3]
            gt_bb = gt_bb[0], gt_bb[1], gt_bb[0] + gt_bb[2], gt_bb[1] + gt_bb[3]

        x11, y11, x12, y12 = tracker_bb
        x21, y21, x22, y22 = gt_bb

        center_1 = np.array(((x11 + x12)/2.0, (y11 + y12)/2.0))
        center_2 = np.array(((x21 + x22)/2.0, (y21 + y22)/2.0))

        euclidean_dist = np.linalg.norm(center_2 - center_1)
        precision.append(euclidean_dist < threshold_precision)

        intersection = max((min(x12, x22) - max(x11, x21)), 0) * max((min(y12, y22) - max(y11, y21)), 0)
        union = ((x12 - x11) * (y12 - y11)) + ((x22 - x21) * (y22 - y21)) - intersection
        iou = intersection * 1.0 / union

        success.append(iou > threshold_success)

    return success, precision


def get_success_precision2(tracker_traj, gt_traj, threshold_precision, threshold_success, yolo=False):
    """
    Definitions
    Success is 
    Precision is the average Euclidean distance between the center locations of the tracked targets and the manually labeled ground truths.
    """
    success = []
    precision = []
    for tracker_bb, gt_bb in zip(tracker_traj, gt_traj):
        if not yolo: 
            tracker_bb = tracker_bb[0], tracker_bb[1], tracker_bb[0] + tracker_bb[2], tracker_bb[1] + tracker_bb[3]
            gt_bb = gt_bb[0], gt_bb[1], gt_bb[0] + gt_bb[2], gt_bb[1] + gt_bb[3]

        x11, y11, w1, h1 = tracker_bb
        x12, y12 = x11 + w1, y11 + h1
        x21, y21, w2, h2 = gt_bb
        x22, y22 = x21 + w2, y21 +h2

        center_1 = np.array(((x11 + x12)/2.0, (y11 + y12)/2.0))
        center_2 = np.array(((x21 + x22)/2.0, (y21 + y22)/2.0))

        euclidean_dist = np.linalg.norm(center_2 - center_1)
        precision.append(euclidean_dist < threshold_precision)

        intersection = max((min(x12, x22) - max(x11, x21)), 0) * max((min(y12, y22) - max(y11, y21)), 0)
        union = ((x12 - x11) * (y12 - y11)) + ((x22 - x21) * (y22 - y21)) - intersection
        iou = intersection * 1.0 / union

        success.append(iou > threshold_success)

    return success, precision



def draw_success_precision(success_ret, name, videos, attr, precision_ret=None, norm_precision_ret=None, bold_name=None, axis=[0, 1]):
    """
    Code adapted from: https://github.com/StrangerZhang/pysot-toolkit/blob/8b5ced3b39129b26b3700c218ffcadba7d8ad278/pysot/visualization/draw_success_precision.py
    """
    COLOR = ((1, 0, 0),
         (0, 1, 0),
         (1, 0, 1),
         (1, 1, 0),
         (0  , 162/255, 232/255),
         (0.5, 0.5, 0.5),
         (0, 0, 1),
         (0, 1, 1),
         (136/255, 0  , 21/255),
         (255/255, 127/255, 39/255),
         (0, 0, 0))

    LINE_STYLE = ['-', '--', ':', '-', '--', ':', '-', '--', ':', '-']
    MARKER_STYLE = ['o', 'v', '<', '*', 'D', 'x', '.', 'x', '<', '.']

    fig, ax = plt.subplots()
    ax.grid(b=True)
    ax.set_aspect(1)
    plt.xlabel('Overlap threshold')
    plt.ylabel('Success rate')
    if attr == 'ALL':
        plt.title(r'\textbf{Success plots of OPE on %s}' % (name))
    else:
        plt.title(r'\textbf{Success plots of OPE - %s}' % (attr))
    plt.axis([0, 1]+axis)
    success = {}
    thresholds = np.arange(0, 1.05, 0.05)
    for tracker_name in success_ret.keys():
        value = [v for k, v in success_ret[tracker_name].items() if k in videos]
        success[tracker_name] = np.mean(value)
    for idx, (tracker_name, auc) in  \
            enumerate(sorted(success.items(), key=lambda x:x[1], reverse=True)):
        if tracker_name == bold_name:
            label = r"\textbf{[%.3f] %s}" % (auc, tracker_name)
        else:
            label = "[%.3f] " % (auc) + tracker_name
        value = [v for k, v in success_ret[tracker_name].items() if k in videos]
        plt.plot(thresholds, np.mean(value, axis=0),
                color=COLOR[idx], linestyle=LINE_STYLE[idx],label=label, linewidth=2)
    ax.legend(loc='lower left', labelspacing=0.2)
    ax.autoscale(enable=True, axis='both', tight=True)
    xmin, xmax, ymin, ymax = plt.axis()
    ax.autoscale(enable=False)
    ymax += 0.03
    ymin = 0
    plt.axis([xmin, xmax, ymin, ymax])
    plt.xticks(np.arange(xmin, xmax+0.01, 0.1))
    plt.yticks(np.arange(ymin, ymax, 0.1))
    ax.set_aspect((xmax - xmin)/(ymax-ymin))
    plt.show()

    if precision_ret:
        # norm precision plot
        fig, ax = plt.subplots()
        ax.grid(b=True)
        ax.set_aspect(50)
        plt.xlabel('Location error threshold')
        plt.ylabel('Precision')
        if attr == 'ALL':
            plt.title(r'\textbf{Precision plots of OPE on %s}' % (name))
        else:
            plt.title(r'\textbf{Precision plots of OPE - %s}' % (attr))
        plt.axis([0, 50]+axis)
        precision = {}
        thresholds = np.arange(0, 51, 1)
        for tracker_name in precision_ret.keys():
            value = [v for k, v in precision_ret[tracker_name].items() if k in videos]
            precision[tracker_name] = np.mean(value, axis=0)[20]
        for idx, (tracker_name, pre) in \
                enumerate(sorted(precision.items(), key=lambda x:x[1], reverse=True)):
            if tracker_name == bold_name:
                label = r"\textbf{[%.3f] %s}" % (pre, tracker_name)
            else:
                label = "[%.3f] " % (pre) + tracker_name
            value = [v for k, v in precision_ret[tracker_name].items() if k in videos]
            plt.plot(thresholds, np.mean(value, axis=0),
                    color=COLOR[idx], linestyle=LINE_STYLE[idx],label=label, linewidth=2)
        ax.legend(loc='lower right', labelspacing=0.2)
        ax.autoscale(enable=True, axis='both', tight=True)
        xmin, xmax, ymin, ymax = plt.axis()
        ax.autoscale(enable=False)
        ymax += 0.03
        ymin = 0
        plt.axis([xmin, xmax, ymin, ymax])
        plt.xticks(np.arange(xmin, xmax+0.01, 5))
        plt.yticks(np.arange(ymin, ymax, 0.1))
        ax.set_aspect((xmax - xmin)/(ymax-ymin))
        plt.show()

    # norm precision plot
    if norm_precision_ret:
        fig, ax = plt.subplots()
        ax.grid(b=True)
        plt.xlabel('Location error threshold')
        plt.ylabel('Precision')
        if attr == 'ALL':
            plt.title(r'\textbf{Normalized Precision plots of OPE on %s}' % (name))
        else:
            plt.title(r'\textbf{Normalized Precision plots of OPE - %s}' % (attr))
        norm_precision = {}
        thresholds = np.arange(0, 51, 1) / 100
        for tracker_name in precision_ret.keys():
            value = [v for k, v in norm_precision_ret[tracker_name].items() if k in videos]
            norm_precision[tracker_name] = np.mean(value, axis=0)[20]
        for idx, (tracker_name, pre) in \
                enumerate(sorted(norm_precision.items(), key=lambda x:x[1], reverse=True)):
            if tracker_name == bold_name:
                label = r"\textbf{[%.3f] %s}" % (pre, tracker_name)
            else:
                label = "[%.3f] " % (pre) + tracker_name
            value = [v for k, v in norm_precision_ret[tracker_name].items() if k in videos]
            plt.plot(thresholds, np.mean(value, axis=0),
                    color=COLOR[idx], linestyle=LINE_STYLE[idx],label=label, linewidth=2)
        ax.legend(loc='lower right', labelspacing=0.2)
        ax.autoscale(enable=True, axis='both', tight=True)
        xmin, xmax, ymin, ymax = plt.axis()
        ax.autoscale(enable=False)
        ymax += 0.03
        ymin = 0
        plt.axis([xmin, xmax, ymin, ymax])
        plt.xticks(np.arange(xmin, xmax+0.01, 0.05))
        plt.yticks(np.arange(ymin, ymax, 0.1))
        ax.set_aspect((xmax - xmin)/(ymax-ymin))
        plt.show()
