import os
import csv
import cv2
import time
import json
import numpy as np
import pandas as pd
import evaluation_helpers as eh
import matplotlib.pyplot as plt
from sct import SCT
from tqdm import tqdm
import seaborn as sns

sns.set()

if __name__ == '__main__':

    with open('configs/tracker.json', 'r') as f:
        cfgs_tracker = json.load(f)

    with open('configs/reid_test.json', 'r') as f:
        cfgs_reid = json.load(f)

    with open('configs/tracker_eval.json', 'r') as f:
        cfgs = json.load(f)

    TRACKERS = {
        "csrt": cv2.TrackerCSRT_create,
        "kcf": cv2.TrackerKCF_create,
        "boosting": cv2.TrackerBoosting_create,
        "mil": cv2.TrackerMIL_create,
        "tld": cv2.TrackerTLD_create,
        "medianflow": cv2.TrackerMedianFlow_create,
        "mosse": cv2.TrackerMOSSE_create,
        "DeepSCT": SCT,
    }

    if cfgs['type'] == 'SOT':
        gt_parser = eh.read_gt_sot
    else:
        gt_parser = eh.read_gt_mot
    
    tracker_parser = eh.read_gt_sot

    stepper = np.arange(0, 1 + cfgs['step_size'], cfgs['step_size'])

    if cfgs['algorithm'] == 'all': algorithms, algorithm_names = TRACKERS.values(), TRACKERS.keys()
    else: algorithms, algorithm_names = [TRACKERS[algorithm] for algorithm in cfgs['algorithm']],  cfgs['algorithm']

    if not os.path.exists(cfgs['save_path']): os.mkdir(cfgs['save_path'])

    sequences = os.path.join(cfgs['read_path'], 'sequences')
    no_seq = cfgs['no_seq'] if cfgs['no_seq'] > 0 else len(sequences)

    if cfgs['eval_sequences'] == 'random':
        eval_sequences = np.random.choice(os.listdir(sequences), no_seq)
    else:
        eval_sequences = cfgs['eval_sequences']

    success_ret = []
    precision_ret = []
    
    for algorithm_name, algorithm in zip(algorithm_names, algorithms):
        success_algorithm = []
        precision_algorithm = []

        for sequence in tqdm(eval_sequences, desc='Evaluating: %s'%algorithm_name):
            success_sequence = []
            precision_sequence = []

            img_dir = os.path.join(sequences, sequence)
            seqlen = len(os.listdir(img_dir))
            tracker_traj = tracker_parser(os.path.join(cfgs['save_path'], sequence, algorithm_name + '_trajectory') + '.csv', seqlen)

            gt_traj = gt_parser(os.path.join(cfgs['read_path'], 'annotations', sequence) + '.txt', seqlen)
            assert len(gt_traj) == len(tracker_traj), "unequal trajectory lengths: gt=%d, tracker=%d"%(len(gt_traj), len(tracker_traj))


            save_path_base = os.path.join(cfgs['save_path'], sequence)
            save_path_algorithm = os.path.join(save_path_base, algorithm_name)
            if not os.path.exists(save_path_base): os.mkdir(save_path_base)

            for threshold in stepper:
                success, precision = eh.get_success_precision(tracker_traj, gt_traj, threshold*50, threshold)            
                success_sequence.append(np.mean(success))
                precision_sequence.append(np.mean(precision))

            success_algorithm.append(success_sequence)
            precision_algorithm.append(precision_sequence)

        success_ret.append(success_algorithm)
        precision_ret.append(precision_algorithm)

    success_ret = np.mean(np.array(success_ret), axis=1)
    precision_ret = np.mean(np.array(precision_ret), axis=1)

    record = []

    for algorithm_name, algorithm_success, algorithm_precision in zip(algorithm_names, success_ret, precision_ret):
        plt.figure(1)
        # g = sns.relplot(kind="line", data=algorithm_success)
        # g.fig.autofmt_xdate()
        # plt.figure(2)
        plt.plot(stepper, algorithm_success, label=algorithm_name+' [%.2f]'%np.mean(algorithm_success))
        plt.figure(2)
        plt.plot(stepper*50, algorithm_precision, label=algorithm_name+' [%.2f]'%np.mean(algorithm_precision))

        if algorithm_name == "DeepSCT":
            temp_success = np.mean(algorithm_success)
            temp_precision = np.mean(algorithm_precision)
        
        percent_increase_success = ((temp_success-np.mean(algorithm_success))/np.mean(algorithm_success))*100
        percent_increase_precision = ((temp_precision-np.mean(algorithm_precision))/np.mean(algorithm_precision))*100
        record.append((algorithm_name, percent_increase_success, percent_increase_precision))

    print(record)
    plt.figure(1)
    plt.xlabel('Overlap threshold')
    plt.ylabel('Success rate')
    plt.title('Success plots of OPE')
    plt.legend()
    plt.savefig("plots/Success-long_term.png", dpi=300)
    plt.figure(2)
    plt.xlabel('Location error threshold')
    plt.ylabel('Precision')
    plt.title('Precision plots of OPE')
    plt.legend()
    plt.savefig("plots/Precision-long_term.png", dpi=300)
    print("Plots Saved!")
    plt.show()