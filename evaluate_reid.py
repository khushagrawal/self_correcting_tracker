import os
import re
import cv2
import csv
import json
import time 
import itertools 
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from reid import ReId


sns.set()


def sorted_alphanumeric(data):
    '''Getting first and last file to get range '''
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(data, key=alphanum_key)

def plot_visualizations(inferences, save_dir):
    incorrect_pos, cnt_pos, incorrect_neg, cnt_neg, inference_time = [], [], [], [], []
    for (_, _, inc_p, cnt_p, inc_n, cnt_n, it) in inferences[1:]:
        incorrect_pos.append(inc_p)
        cnt_pos.append(cnt_p)
        incorrect_neg.append(inc_n)
        cnt_neg.append(cnt_n)
        inference_time.append(it)

    p = np.array(cnt_pos)
    Fn = np.array(incorrect_pos)
    print(p, Fn)
    Tp = p - Fn

    n = np.array(cnt_neg)
    Fp = np.array(incorrect_neg)
    Tn = n - Fp

    inference_time = np.array(inference_time)

    accuracy = (Tp + Tn) / (Tp + Tn + Fp + Fn)
    precision = (Tp) / (Tp + Fp)
    recall = (Tp) / (Tp + Tn)
    f1 = 2 * (precision * recall) / (precision + recall)

    plt.figure(1)
    metrics = [['accuracy', i] for i in accuracy] + \
              [['precision', i] for i in precision] + \
              [['recall', i] for i in recall] + \
              [['f1', i] for i in f1]

    print(metrics)

    df = pd.DataFrame(metrics, columns=['metrics','percent'])
    ax = sns.barplot(x='metrics', y='percent', data=df, capsize=.2)
    plt.savefig(os.path.join(save_dir, 'evaluation.png'), dpi=300, bbox_inches='tight')
    plt.show()


if __name__ == '__main__':

    with open('configs/reid_test.json', 'r') as f:
        cfgs = json.load(f)

    if not os.path.exists(cfgs['save_dir']): os.mkdir(cfgs['save_dir'])
    save_path = os.path.join(cfgs['save_dir'], 'inferences.csv')

    data = pd.DataFrame()

    files = os.listdir(cfgs['dataset_path'])
    pairs = itertools.permutations(files, 2)

    inferences = [['pos seq', 'neg seq', 'total FN', 'total pos eg', 'total FP', 'total neg eg', 'inference time (s)']]

    reid = ReId(cfgs['reid'])

    for (pos, neg) in list(pairs):
        if cfgs['debug']: print('Positive: %s | Negative: %s'%(pos, neg))

        seq_path_pos = os.path.join(cfgs['dataset_path'], pos)
        seq_path_neg = os.path.join(cfgs['dataset_path'], neg)

        seq_pos = sorted_alphanumeric(os.listdir(seq_path_pos))
        seq_neg = sorted_alphanumeric(os.listdir(seq_path_neg))

        first_number_pos = int(seq_pos[0].split('_')[1].split('.')[0])
        last_number_pos = int(seq_pos[-1].split('_')[1].split('.')[0])
        if cfgs['debug']: print('Positive: %s to %s'%(first_number_pos, last_number_pos))

        first_number_neg = int(seq_neg[0].split('_')[1].split('.')[0])
        last_number_neg = int(seq_neg[-1].split('_')[1].split('.')[0])
        if cfgs['debug']: print('Negative: %s to %s'%(first_number_neg, last_number_neg))

        len_pos = len(os.listdir(seq_path_pos))
        len_neg = len(os.listdir(seq_path_neg))

        img = cv2.imread(os.path.join(seq_path_pos, 'person_'+str(first_number_pos)+'.jpg'))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    
        
        reid.update(img)

        cnt_pos = 0
        cnt_neg = 0
        start = time.time()

        print('Evaluating positive examples')
        for i in range(first_number_pos+1, last_number_pos+1):
            img = cv2.imread(os.path.join(seq_path_pos, 'person_' + str(i) + '.jpg'))
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            match, features, diff = reid(img)
            if not match: cnt_pos += 1
            if cfgs['debug']: print(diff)
                
            # if cffeature_update is not None and 1%feature_update == 0:
            #     features_target = features

        print('Evaluating negative examples')    
        for i in range(first_number_neg, last_number_neg):
            img = cv2.imread(os.path.join(seq_path_neg, 'person_' + str(i) + '.jpg'))
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            match, features, diff = reid(img)
            if match: cnt_neg += 1
            if cfgs['debug']: print(diff)

        inference_time = (time.time() - start) / (len_pos + len_neg)
        inferences.append([seq_path_pos, seq_path_neg, cnt_pos, len_pos, cnt_neg, len_neg, inference_time])

        if cfgs['debug']: print('Feature Shape: %s'%str(features.shape))
        print('Average inference time: %.3f s'%(inference_time))
        print('False negatives: %s/%s | False positives: %s/%s'%(cnt_pos, len_pos, cnt_neg, len_neg))

        with open(save_path, 'w') as f:
            write = csv.writer(f)
            write.writerows(inferences)
        
    plot_visualizations(inferences, cfgs['save_dir'])