import os
import cv2
import time
import json
import torchreid
import numpy as np
import torch.nn.functional as f
from torchreid.utils import FeatureExtractor

class ReId():
    def __init__(self, cfgs):
        self.cfgs = cfgs
        self.metric = f.cosine_similarity if self.cfgs['metric'] == 'cosine' else f.mse_loss
        self.threshold = self.cfgs['threshold']
        self.extractor = FeatureExtractor(model_name=self.cfgs['model_name'], model_path=self.cfgs['model_path'], device=self.cfgs['device'])
 
    def __call__(self, img, update=False):

        features = self.extractor(img)
        diff = self.metric(features, self.features_target)
        match = False if (self.cfgs['metric'] == 'cosine' and diff < self.threshold) or (self.cfgs['metric'] == 'mse' and diff > self.threshold) else True
        
        if update: self.features_target = features

        return match, features, diff

    def update(self, img):
        self.features_target = self.extractor(img)

    def set_threshold(self, neg_dir, pos_dir):
        pos_seqs = os.listdir(pos_dir)
        neg_seqs = os.listdir(neg_dir)

        neg_diffs = []
        pos_diffs = []

        pos_init = np.random.choice(neg_seqs)
        img = cv2.imread(os.path.join(pos_dir, pos_init))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        features_target = self.extractor(img)

        for i in pos_seqs:
            img = cv2.imread(os.path.join(pos_dir, i))
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            features = extractor(img)
            diff = self.metric(features, features_target)

            pos_diffs.append(diff)
            
        for i in neg_seqs:
            img = cv2.imread(os.path.join(neg_dir, i))
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            features = extractor(img)
            diff = self.metric(features, features_target)

            neg_diffs.append(diff)

        
        pos_diffs = np.array(pos_diffs)
        neg_diffs = np.array(neg_diffs)
        pos_mean = np.mean(pos_diffs)
        neg_mean = np.mean(neg_diffs)
        self.threshold = (neg_mean + pos_mean)/2.0

        if self.cfgs['cosine']:
            print('Problem with automatic threshold') if ((pos_diffs < self.threshold).any() or (neg_diffs > self.threshold).any()) else print('positive check')
        else:
            print('Problem with automatic threshold') if ((pos_diffs > self.threshold).any() or (neg_diffs < self.threshold).any()) else print('negative check')
