import os
import cv2 
import shutil
import csv
import argparse
import numpy as np


def read_gt(path):
    '''Read the ground truth file from the give path'''
    gt = []
    with open(path) as f:
        c = csv.reader(f, delimiter=',')
        for row in c:
            gt.append(row)
    gt = np.array(gt).astype(np.int)
    return gt


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Create dataset for training re-id dataset from the VisDrone Dataset')
    parser.add_argument("-r", "--read_path", type=str, default='/home/rex/Downloads/VisDrone2019-SOT-train/', help='Path the VisDrone dataset')
    parser.add_argument("-s", "--save_path", type=str, default='dataset_reid_sot', help='Path for saving processed dataset')
    parser.add_argument("-d", "--dimension", type=int, nargs='+', default=None, help='height width for the images to be saved')
    parser.add_argument("-c", "--class_name", type=str, default='person', help='Class name for the processed dataset')
    parser.add_argument("-v", "--invalid_sequences", type=str, nargs='+', default=['uav0000003_00000_s', 'uav0000090_01104_s', 'uav0000091_01288_s', 'uav0000076_00241_s', 'uav0000090_00276_s', 'uav0000091_00460_s', 'uav0000107_01763_s', 'uav0000089_00920_s', 'uav0000091_01035_s', 'uav0000091_02530_s', 'uav0000126_07915_s'])

    args = parser.parse_args()

    if not os.path.exists(args.save_path): os.mkdir(args.save_path)

    seq_dir = os.path.join(args.read_path, 'sequences')
    sequences = os.listdir(seq_dir)
    identity = 0

    for seq in sequences:

        if seq in args.invalid_sequences:
            continue


        print('Processing: %s'%seq)
        save_path = os.path.join(args.save_path, str(seq))

        # Recreate the directory for the given sequence
        if os.path.exists(save_path): shutil.rmtree(save_path)
        os.mkdir(save_path)

        labels = read_gt(os.path.join(args.read_path, 'annotations', seq + '.txt'))

        for index in range(1, len(labels)):
            img_file = os.path.join(seq_dir, seq, 'img%07d.jpg'%index)
            frame = cv2.imread(img_file)

            x, y, w, h = labels[index-1]
            cropped_frame = frame[y:y+h, x:x+w, :]
            
            if args.dimension is not None:
                height, width = args.dimension
                cropped_frame = cv2.resize(cropped_frame, (width, height))

            cv2.imwrite(os.path.join(save_path, args.class_name + '_' + str(index) + '.jpg'), cropped_frame)

    print('Done!')


